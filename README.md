# Обработка изображений ПЗ4

## Состав репозитория

В [static](static/) хранятся изображения, видосы и гифки.
В [scripts](scripts/) хранятся код и блокноты

# Задание для новеньких

Добавил 2 слоя с линейной трансформацией Linear и функцию активации Relu после первого слоя

```
class SimpleNet(nn.Module):
    def __init__(self):
        super(SimpleNet, self).__init__()
        self.fc1 = nn.Linear(28 * 28, 128)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 10)
    def forward(self, x):
      x = torch.flatten(x, 1)
      x = self.relu(self.fc1(x))
      x = self.relu(self.fc2(x))
      x = self.fc3(x)
      return x
```

Разбавил датасет с помощью Transform

```
transform = transforms.Compose([
    transforms.RandomRotation(10),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.5,), (0.5,))
])
```

Запустил обучение на 14 эпох.
[Блокнот](scripts/dima-blowup/img_process_4_easy.ipynb) с легким заданием

## :b: Задание на плюсик

Артем сдался и сделал на битоне...

[Блокнот](scripts/zimain-lonely-lore/img_process_4.ipynb) с заданием для смешариков

### Архитектура нейронки

<img src="static/fashionMnistClassifier.onnx.png" width="100%" />

### Метрики

Для визуализации метрик завел переменную history, где храню данные в виде json

<img src="static/Metrics.png" width="500px" />

### Выводы

Никогда не пытайтесь запустить обучение на затыке вида MX250, обучайте либо на норм желе, либо на облаке.
Артем устал
